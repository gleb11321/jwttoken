﻿namespace testAPI_2.Models
{
    public class AuthenticatedResponse
    {
        public string? Token { get; set; }
    }
}
